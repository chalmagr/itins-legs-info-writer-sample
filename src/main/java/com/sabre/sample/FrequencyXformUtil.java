package com.sabre.sample;


import com.sabre.fltsked.extensions.modules.profit.ds.DayOfWeek;
import com.sabre.fltsked.extensions.modules.profit.ds.Frequency;

import java.util.HashSet;

import static com.sabre.sample.InfoRWUtil.FLD_DELIMITER;

public class FrequencyXformUtil
{
    public String xToString(Frequency frequency)
    {
        final StringBuilder freqStr = new StringBuilder();
        for (DayOfWeek day : frequency.asSet()) {
            freqStr.append(day).append(FLD_DELIMITER);
        }

        freqStr.deleteCharAt(freqStr.length() - 1);  //remove last delimiter
        return freqStr.toString();
    }

    public Frequency parseFrequency(String freqStr)
    {
        final String[] daysOfWeek = freqStr.split(FLD_DELIMITER);
        final HashSet<DayOfWeek> days = new HashSet<DayOfWeek>();
        for (String day : daysOfWeek) {
            days.add(DayOfWeek.valueOf(day));
        }
        return Frequency.create(days);
    }
}
