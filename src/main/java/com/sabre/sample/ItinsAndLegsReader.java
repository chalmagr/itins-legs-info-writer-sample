package com.sabre.sample;

import com.sabre.fltsked.extensions.modules.profit.ds.connectionbuilder.Itinerary;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class ItinsAndLegsReader
{
    private final ItinXformUtil itinXformUtil = new ItinXformUtil();

    public static void main(String[] args) throws IOException
    {
        String filePath = args[0];
        BufferedReader reader = null;
        try
        {
            reader = new BufferedReader(new FileReader(new File(filePath)));

            final ItinsAndLegsReader itinsAndLegsReader = new ItinsAndLegsReader();
            Map<String, List<Itinerary>> itinsByMarket = itinsAndLegsReader.readItins(reader);
        }
        finally
        {
            if(reader != null)
            {
                reader.close();
            }
        }
    }

    public Map<String, List<Itinerary>> readItins(BufferedReader reader) throws IOException
    {
        final Map<String, List<Itinerary>> itinsMap = new HashMap<String, List<Itinerary>>();
        final String header = reader.readLine();
        String line;
        while ((line = reader.readLine()) != null && line.trim().length() != 0) {
            final String[] marketNSize = line.split(":");
            final String market = marketNSize[1];
            final int itinSize = Integer.parseInt(marketNSize[3]);

            List<Itinerary> itineraries = itinsMap.get(market);
            if (itineraries == null) {
                itineraries = new ArrayList<Itinerary>();
                itinsMap.put(market, itineraries);
            }
            for (int idx = 0; idx < itinSize; idx++) {
                itineraries.add(itinXformUtil.parseItin(reader.readLine()));
            }
        }

        return itinsMap;
    }






}
