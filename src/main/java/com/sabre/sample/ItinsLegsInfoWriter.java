package com.sabre.sample;

import com.sabre.fltsked.extensions.modules.profit.ConnectionEditing;
import com.sabre.fltsked.extensions.modules.profit.ds.Frequency;
import com.sabre.fltsked.extensions.modules.profit.ds.connectionbuilder.Itinerary;
import org.apache.commons.collections.IteratorUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ItinsLegsInfoWriter implements ConnectionEditing
{
    private BufferedWriter writer = null;
    private ItinXformUtil itinXformer = new ItinXformUtil();

    public ItinsLegsInfoWriter()
    {
    }

    private ItinsLegsInfoWriter(BufferedWriter writer)
    {
        this.writer = writer;
    }

    static ItinsLegsInfoWriter forTest(BufferedWriter writer)
    {
        return new ItinsLegsInfoWriter(writer);
    }

    @Override
    public Map<Itinerary, Frequency> getValidFrequencies(String origin, String destination, Iterable<? extends Itinerary> itineraries) throws Exception
    {
        synchronized (writer)
        {
            final List itinList = IteratorUtils.toList(itineraries.iterator());
            writer.write("Market:" + origin + destination + ":Size:" + itinList.size());
            writer.newLine();

            for (Itinerary itin : itineraries)
            {
                writer.write(itinXformer.xToString(itin));
                writer.newLine();
            }
            return Collections.emptyMap();
        }
    }

    @Override
    public void onStart(long analysisId, File worksetDir) throws Exception
    {
        writer = new BufferedWriter(new FileWriter(new File(worksetDir, "ItinLegInfo.dat")));
        writer.write(itinXformer.getHeader());
        writer.newLine();
    }

    @Override
    public void onCompleted() throws Exception
    {
        if (writer != null)
        {
            writer.flush();
            writer.close();
        }
    }
}