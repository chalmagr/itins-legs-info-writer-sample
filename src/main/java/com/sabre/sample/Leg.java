package com.sabre.sample;


import com.sabre.fltsked.extensions.modules.profit.ds.Frequency;
import com.sabre.fltsked.extensions.modules.profit.ds.connectionbuilder.FlightLeg;

public class Leg implements FlightLeg
{
    private final String airline;
    private final String flightNumber;
    private final char flightNumberSuffix;
    private final String deptArp;
    private final String arrArp;
    private final int distance;
    private final int deptTime;
    private final int arrTime;
    private final int elapsedTime;
    private final Frequency frequency;
    private final boolean isOperational;
    private final String opAirline;
    private final String opFlightNumber;

    public Leg(String airline, String flightNumber, char flightNumberSuffix, String deptArp, String arrArp, int distance, int deptTime, int arrTime,
        int elapsedTime, Frequency frequency, boolean isOperational, String opAirline, String opFlightNumber)
    {
        this.airline = airline;
        this.flightNumber = flightNumber;
        this.flightNumberSuffix = flightNumberSuffix;
        this.deptArp = deptArp;
        this.arrArp = arrArp;
        this.distance = distance;
        this.deptTime = deptTime;
        this.arrTime = arrTime;
        this.elapsedTime = elapsedTime;
        this.frequency = frequency;
        this.isOperational = isOperational;
        this.opAirline = opAirline;
        this.opFlightNumber = opFlightNumber;
    }

    @Override
    public String getAirline()
    {
        return airline;
    }

    @Override
    public String getFlightNumber()
    {
        return flightNumber;
    }

    @Override
    public char getFlightNumberSuffix()
    {
        return flightNumberSuffix;
    }

    @Override
    public Frequency getFrequency()
    {
        return frequency;
    }

    @Override
    public String getDeptArp()
    {
        return deptArp;
    }

    @Override
    public String getArvlArp()
    {
        return arrArp;
    }

    @Override
    public int getDistance()
    {
        return distance;
    }

    @Override
    public int getDeptTime()
    {
        return deptTime;
    }

    @Override
    public int getArvlTime()
    {
        return arrTime;
    }

    @Override
    public int getElapsedTime()
    {
        return elapsedTime;
    }

    @Override
    public boolean isOperational()
    {
        return isOperational;
    }

    @Override
    public String getOperationalAirline()
    {
        return opAirline;
    }

    @Override
    public String getOperationalFlightNumber()
    {
        return opFlightNumber;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Leg)) return false;

        Leg leg = (Leg) o;

        if (arrTime != leg.arrTime) return false;
        if (deptTime != leg.deptTime) return false;
        if (distance != leg.distance) return false;
        if (elapsedTime != leg.elapsedTime) return false;
        if (flightNumberSuffix != leg.flightNumberSuffix) return false;
        if (isOperational != leg.isOperational) return false;
        if (!airline.equals(leg.airline)) return false;
        if (!arrArp.equals(leg.arrArp)) return false;
        if (!deptArp.equals(leg.deptArp)) return false;
        if (!flightNumber.equals(leg.flightNumber)) return false;
        if (!frequency.equals(leg.frequency)) return false;
        if (!opAirline.equals(leg.opAirline)) return false;
        if (!opFlightNumber.equals(leg.opFlightNumber)) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = airline.hashCode();
        result = 31 * result + flightNumber.hashCode();
        result = 31 * result + (int) flightNumberSuffix;
        result = 31 * result + deptArp.hashCode();
        result = 31 * result + arrArp.hashCode();
        result = 31 * result + distance;
        result = 31 * result + deptTime;
        result = 31 * result + arrTime;
        result = 31 * result + elapsedTime;
        result = 31 * result + frequency.hashCode();
        result = 31 * result + (isOperational ? 1 : 0);
        result = 31 * result + opAirline.hashCode();
        result = 31 * result + opFlightNumber.hashCode();
        return result;
    }
}
