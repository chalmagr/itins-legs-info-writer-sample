package com.sabre.sample;

import com.sabre.fltsked.extensions.modules.profit.ds.Frequency;
import com.sabre.fltsked.extensions.modules.profit.ds.connectionbuilder.FlightLeg;

import static com.sabre.sample.InfoRWUtil.COMMA;

public class LegXformUtil
{

    private final FrequencyXformUtil freqXformer = new FrequencyXformUtil();

    String getHeader()
    {
        return "Origin, Destination, Airline, Flight Number, Flight Number Suffix, Frequency, Dept Time, Arr Time, Elapsed Time, Distance, Is Operational, Op Airline, Op Flight Number";
    }

    String xToString(FlightLeg leg)
    {
        final StringBuilder legStr = new StringBuilder();
        legStr.append(leg.getDeptArp()).append(COMMA);
        legStr.append(leg.getArvlArp()).append(COMMA);
        legStr.append(leg.getAirline()).append(COMMA);
        legStr.append(leg.getFlightNumber()).append(COMMA);
        legStr.append(leg.getFlightNumberSuffix()).append(COMMA);
        legStr.append(freqXformer.xToString(leg.getFrequency())).append(COMMA);
        legStr.append(leg.getDeptTime()).append(COMMA);
        legStr.append(leg.getArvlTime()).append(COMMA);
        legStr.append(leg.getElapsedTime()).append(COMMA);
        legStr.append(leg.getDistance()).append(COMMA);
        legStr.append(String.valueOf(leg.isOperational())).append(COMMA);
        legStr.append(leg.getOperationalAirline()).append(COMMA);
        legStr.append(leg.getOperationalFlightNumber());
        return legStr.toString();
    }

    public FlightLeg parseLeg(String legInfo)
    {
        final String[] legStr = legInfo.split(InfoRWUtil.COMMA);
        final String deptArp = legStr[0];
        final String arrArp = legStr[1];
        final String airline = legStr[2];
        final String flightNumber = legStr[3];
        final char flightNumberSuffix = legStr[4].charAt(0);
        final Frequency frequency = freqXformer.parseFrequency(legStr[5]);
        final int deptTime = Integer.parseInt(legStr[6]);
        final int arrTime = Integer.parseInt(legStr[7]);
        final int elapsedTime = Integer.parseInt(legStr[8]);
        final int distance = Integer.parseInt(legStr[9]);
        final boolean isOp = Boolean.valueOf(legStr[10]);
        final String opAirline = legStr[11];
        final String opFlightNumber = legStr[12];
        return new Leg(airline, flightNumber, flightNumberSuffix, deptArp, arrArp, distance, deptTime, arrTime, elapsedTime, frequency, isOp, opAirline, opFlightNumber);
    }
}
