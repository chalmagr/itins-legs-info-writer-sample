package com.sabre.sample;

import com.sabre.fltsked.extensions.modules.profit.ds.DayOfWeek;
import com.sabre.fltsked.extensions.modules.profit.ds.Frequency;
import com.sabre.fltsked.extensions.modules.profit.ds.connectionbuilder.FlightLeg;
import com.sabre.fltsked.extensions.modules.profit.ds.connectionbuilder.Itinerary;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class ItinXformUtilTest extends TestCase
{
    private Frequency frequency;
    private Leg leg1, leg2;
    private Itin itin;
    private final String expectedItinStr = "DFW,FRA,SUN#MON#TUE,100,1400,1200,8000,2,[DFW,JFK,UA,123, ,SUN#MON#TUE,100,600,500,2000,true,UA,123][JFK,FRA,UA,111, ,SUN#MON#TUE,700,1400,700,6000,false,LH,333]";

    @Override
    public void setUp() throws Exception
    {
        frequency = Frequency.create(DayOfWeek.SUN, DayOfWeek.MON, DayOfWeek.TUE);
        leg1 = new Leg("UA", "123", ' ', "DFW", "JFK", 2000, 100, 600, 500, frequency, true, "UA", "123");
        leg2 = new Leg("UA", "111", ' ', "JFK", "FRA", 6000, 700, 1400, 700, frequency, false, "LH", "333");
        final List<FlightLeg> legs = new ArrayList<FlightLeg>();
        legs.add(leg1);
        legs.add(leg2);

        itin = new Itin("DFW", "FRA", 100, 1400, 1200, 8000, frequency, legs);
    }

    public void testXToString() throws Exception
    {
        final ItinXformUtil testedObj = new ItinXformUtil();
        final String itinString = testedObj.xToString(itin);
        assertEquals("Incorrect string value for itin", expectedItinStr, itinString);
    }

    public void testParseItin() throws Exception
    {
        final ItinXformUtil testedObj = new ItinXformUtil();
        final Itinerary itinResult = testedObj.parseItin(expectedItinStr);
        assertEquals("Incorrect itin", itin, itinResult);
    }
}
