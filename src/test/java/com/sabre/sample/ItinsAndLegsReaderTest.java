package com.sabre.sample;

import com.sabre.fltsked.extensions.modules.profit.ds.DayOfWeek;
import com.sabre.fltsked.extensions.modules.profit.ds.Frequency;
import com.sabre.fltsked.extensions.modules.profit.ds.connectionbuilder.FlightLeg;
import com.sabre.fltsked.extensions.modules.profit.ds.connectionbuilder.Itinerary;
import junit.framework.TestCase;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ItinsAndLegsReaderTest extends TestCase
{

    public void testReader() throws Exception
    {
        final Frequency frequency = Frequency.create(DayOfWeek.SUN, DayOfWeek.MON, DayOfWeek.TUE);
        final Leg leg1 = new Leg("UA", "123", ' ', "DFW", "JFK", 2000, 100, 600, 500, frequency, false, "LH", "321");
        final Leg leg2 = new Leg("UA", "111", ' ', "JFK", "FRA", 6000, 700, 1400, 700, frequency, false, "LH", "333");
        final List<FlightLeg> legs = new ArrayList<FlightLeg>();
        legs.add(leg1);
        legs.add(leg2);

        final Itin itin = new Itin("DFW", "FRA", 100, 1400, 1200, 8000, frequency, legs);
        final List<Itin> itins = new ArrayList<Itin>();
        itins.add(itin);

        final StringWriter outStream = new StringWriter();
        final BufferedWriter writer = new BufferedWriter(outStream);
        writer.newLine();
        final ItinsLegsInfoWriter itinLegWriter = ItinsLegsInfoWriter.forTest(writer);
        itinLegWriter.getValidFrequencies("DFW", "FRA", itins);
        itinLegWriter.onCompleted();

        final BufferedReader reader = new BufferedReader(new StringReader(outStream.toString()));

        final Map<String, List<Itinerary>> results = new ItinsAndLegsReader().readItins(reader);
        assertEquals("Expected 1 itin", 1, results.get("DFWFRA").size());
        assertEquals("Incorrect itin", itin, results.get("DFWFRA").get(0));
    }
}
