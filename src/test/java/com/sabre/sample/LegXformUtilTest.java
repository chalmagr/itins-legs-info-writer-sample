package com.sabre.sample;

import com.sabre.fltsked.extensions.modules.profit.ds.DayOfWeek;
import com.sabre.fltsked.extensions.modules.profit.ds.Frequency;
import com.sabre.fltsked.extensions.modules.profit.ds.connectionbuilder.FlightLeg;
import junit.framework.TestCase;

public class LegXformUtilTest extends TestCase
{
    private Frequency frequency;
    private Leg leg;

    private final String expectedLegStr = "DFW,JFK,UA,123, ,SUN#MON#TUE,100,600,500,2000,true,UA,123";

    @Override
    public void setUp() throws Exception
    {
        frequency = Frequency.create(DayOfWeek.SUN, DayOfWeek.MON, DayOfWeek.TUE);
        leg = new Leg("UA", "123", ' ', "DFW", "JFK", 2000, 100, 600, 500, frequency, true, "UA", "123");
    }

    public void testXToString() throws Exception
    {
        final LegXformUtil testedObj = new LegXformUtil();
        final String legString = testedObj.xToString(leg);
        assertEquals("Incorrect string value for leg", expectedLegStr, legString);
    }

    public void testParseLeg() throws Exception
    {
        final LegXformUtil testedObj = new LegXformUtil();
        final FlightLeg legResult = testedObj.parseLeg(expectedLegStr);
        assertEquals("Incorrect leg", leg, legResult);
    }
}
